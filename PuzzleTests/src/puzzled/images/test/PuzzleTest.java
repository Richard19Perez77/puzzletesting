package puzzled.images.test;

import puzzled.images.activity.MainActivity;
import puzzled.images.surface.PuzzleSurface;
import android.test.ActivityInstrumentationTestCase2;

public class PuzzleTest<MyFirstTestActivity> extends
		ActivityInstrumentationTestCase2<MainActivity> {

	private MainActivity myMainActivity;
	final int EASY = 9;

	public PuzzleTest() {
		super(MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		myMainActivity = getActivity();
	}

	public void testPreconditions() {
		assertNotNull("mFirstTestActivity is null", myMainActivity);
	}

	public void testAfterSetup() {
		// check for variables to be set in place after a fresh start
		assertNotNull("myContext is null", myMainActivity.getBaseContext());

		// check the audio stream receiver is created
		assertNotNull("noisy audio stream receiver is not null",
				myMainActivity.myNoisyAudioStreamReceiver);

		assertFalse("resuming puzzle when shouldn't",
				myMainActivity.common.resumePreviousPuzzle);

		// test reference UI components by making sure they are not null
		assertNotNull("puzzle surfac is null", myMainActivity.puzzleSurface);

		assertNotNull("puzzle thread is null", myMainActivity.puzzleThread);

		assertNotNull("status textview is not null",
				myMainActivity.common.mStatusText);

		assertNotNull("bottom left button is null",
				myMainActivity.common.leftWebLinkButton);

		assertNotNull("bottom right button is null",
				myMainActivity.common.rightWebLinkButton);

		assertNotNull("MediaPlayer is null", myMainActivity.myMediaPlayer);

		assertNotNull("Sound Pool is null", myMainActivity.mySoundPool);

		assertNotNull("Audio Manager is null", myMainActivity.audioManager);

		assertEquals(myMainActivity.puzzleThread.mMode,
				PuzzleSurface.STATE_PAUSE);

	}
}