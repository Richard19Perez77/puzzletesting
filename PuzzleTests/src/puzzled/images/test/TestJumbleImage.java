package puzzled.images.test;

import puzzled.images.activity.MainActivity;
import android.test.ActivityInstrumentationTestCase2;

public class TestJumbleImage<MyFirstTestActivity> extends
ActivityInstrumentationTestCase2<MainActivity> {

	private MainActivity myMainActivity;
	final int EASY = 9;

	public TestJumbleImage() {
		super(MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		myMainActivity = getActivity();
	}
	
	//check that every piece is in a different slot
	public void testPuzzleSetup() {
		// test that the puzzle is sorted
		for (int i = 0; i < EASY; i++) {
			assertTrue(myMainActivity.common.puzzleSlots[i].slotNumber == myMainActivity.common.puzzlePieces[i].pieceNumber);
		}
	}
}